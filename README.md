# KmerFinderJS [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage Status](https://coveralls.io/repos/josl/kmerjs/badge.svg?branch=master&service=github)](https://coveralls.io/github/josl/kmerjs?branch=master)


## Description

KmerFinderJS is a methos based on K-mer statistics for identifying bacterial species in whole genome data. This method is based on a previous one called KmerFinder that as a web server that have been used more than 10.000 times.

KmerFinderJS is a new development where the previous method has been rewritten in JavaScript, which allows the program to be executed in a client- server environment. KmerFinderJS also replaces the python based hash structure for holding the databases with a true key-value database called Redis.

## Install dependencies

```sh
$ npm install --save
```

## External dependencies

A working [Redis](https://redis.io) database should be available.

## Usage

First we need to create a kmer database. Instrucctions can be found in the following repository:

[KmerDB](https://bitbucket.org/genomicepidemiology/kmerdb)

The scripts to create the Redis DB expect that the files created byt KmerDB follow this naming scheme: bacteria.organisms.ATGAC

The following scripts will transforms the kmer databases created by kmerdb into two text files that will be later imported to Redis following [this guide](https://redis.io/topics/mass-insert)

This script will create the summary entry:

```sh
python scripts/summaryDB.py
cat bacteria_organism_summary.txt | redis-cli --pipe -n 4
```
The summary needs to be imported to the Redis DB collection numbered 4

```sh
python scripts/kmerDBtoRedis.py
cat bacteria_organism.txt | redis-cli --pipe -n 3
```
The kmer db needs to be imported to the Redis collection DB numbered 3

To create the browser version:
```sh
gulp babel
browserify dist/kmers.js  -o path/to/kmers.js -t [ babelify --presets [ es2015] ] -s kmerModule
browserify dist/kmerFinderClient.js  -o path/to/KMF.js -t [ babelify --presets [es2015] ] -s kmerModule
regenerator --include-runtime path/to/KMF.js > path/to/kmerFinder.js
rm path/to/KMF.js
```
To import it in a JavaScript file:
```js
var kmerjs = new kmerModule.KmerFinderClient(
    file, 'browser', 'ATGAC', 16, 1, 1, true
);

```

To run the command line version
```sh
node --max_old_space_size=8192 lib/cli.js -f /path/to/file.fastq -o 1 -u REDIS_URL
```

## License
Apache-2.0 © [Jose Luis Bellod Cisneros](http://josl.github.io)

[npm-image]: https://badge.fury.io/js/kmerjs.svg
[npm-url]: https://npmjs.org/package/kmerjs
[travis-image]: https://travis-ci.org/josl/kmerjs.svg?branch=master
[travis-url]: https://travis-ci.org/josl/kmerjs
[daviddm-image]: https://david-dm.org/josl/kmerjs.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/josl/kmerjs
[coveralls-image]: https://coveralls.io/repos/josl/kmerjs/badge.svg
[coveralls-url]: https://coveralls.io/r/josl/kmerjs
