#!/usr/bin/env sh

# It will create a CSV file with the following format:
# file_name, total_time, kmer_extract, file_size, kmer_map_size, reduced_db_size, db_query_time, wta_time

PATH=$1

# Loop over all file and execute all
for file in ${PATH}/*
do
    name=${file##*/}
    base=${name%.txt}

    # Python call
    /Users/cisneror/anaconda2/bin/python2.7 /Users/cisneror/code/genomic-git/kmerfinder/kmer-env/kmerdb-bitbucket/findtemplate.py -i ${file} -t /Users/cisneror/code/genomic-git/kmerfinder/kmer-env/kmerdb-bitbucket/database/complete_genomes.ATGAC -x "ATGAC" -w -o ~/code/genomic-git/kmerjsDocker/database/python/${name}.output
done
