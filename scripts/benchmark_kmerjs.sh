#!/usr/bin/env sh

# It will create a CSV file with the following format:
# file_name, total_time, kmer_extract, file_size, kmer_map_size, reduced_db_size, db_query_time, wta_time

PATH=$1

# Loop over all file and execute all
for file in ${PATH}/*
do
    name=${file##*/}
    base=${name%.txt}
    # KmerJS call
    # The option -o will output the following:
    # - Time Extracting kmers
    # - Size of KmerMap
    # - Time querying Database
    # - Size of reduced Database
    # - Time of WTA
    # - Total time
    # /usr/local/Cellar/node/6.8.1/bin/node --max_old_space_size=8192 lib/cli.js -f ${file} -o 0 -t 1 -d KmerBacteria -u redis://192.168.99.100:6379
    /usr/local/Cellar/node/6.8.1/bin/node --max_old_space_size=8192 lib/cli.js -f ${file} -o 0 -t 1 -d KmerBacteria -u redis://192.168.99.100:6379
done
